package machine;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */
public class VendingMachine {
	
	/*TODO: (With regards to requirements) 
	 * Track coin count
	 * Track item Count
	 * Refactor code defining and manipulating items
	 * 
	 * Leaving incomplete due to instructions: 
	 * 
	 * "You don�t need to tackle the whole problem. We think someone with reasonable experience of TDD should be able to 
	 * demonstrate the skills that we�re looking for in around an hour by satisfying a reasonable subset of the stated requirements."
	 * 
	 * */
	
	private boolean isMachineOn = false;
	private double currentInsertedBalance = 0.0;
	
	private int itemACount = 0;
	private double itemAPrice = 0.6;
	private int itemBCount = 0;
	private double itemBPrice = 1.0;
	private int itemCCount = 0;
	private double itemCPrice = 1.7;
	
	public VendingMachine() {
		super();
	}
	
	public boolean isOn() {
		return isMachineOn;
	}
	
	public void setOn() {
		isMachineOn = true;
	}
	
	public void setOff() {
		isMachineOn = false;
	}
	
	public void insertMoney(double coin) {
		if(isMachineOn && Coin.valueOf(coin).isVendingMachineAccepted()) {
			currentInsertedBalance+= coin;
		}
	}
	
	public double getCurrentInsertedMoney() {
		return currentInsertedBalance;
	}
	
	//Coin Return
	public double refundCurrentInsertedBalance() {
		double refund = currentInsertedBalance;
		currentInsertedBalance = 0.0;
		return refund;
	}
	
	public void vendItemA() {
		if(currentInsertedBalance >= itemAPrice && itemACount > 0) {
			setItemACount(getItemACount() - 1);
			updateCurrentInsertedBalance(itemAPrice);
		}
	}

	public int getItemACount() {
		return itemACount;
	}

	public void setItemACount(int itemACount) {
		this.itemACount = itemACount;
	}
	
	private void updateCurrentInsertedBalance(double itemPrice) {
		currentInsertedBalance -= itemPrice;
	}

	public int getItemBCount() {
		return itemBCount;
	}

	public void setItemBCount(int itemBCount) {
		this.itemBCount = itemBCount;
	}

	public int getItemCCount() {
		return itemCCount;
	}

	public void setItemCCount(int itemCCount) {
		this.itemCCount = itemCCount;
	}
	
	public void vendItemB() {
		if(currentInsertedBalance >= itemBPrice && itemBCount > 0) {
			setItemBCount(getItemBCount() - 1);
			updateCurrentInsertedBalance(itemBPrice);
		}
	}
	
	public void vendItemC() {
		if(currentInsertedBalance >= itemCPrice) {
			setItemCCount(getItemCCount() - 1);
			updateCurrentInsertedBalance(itemCPrice);
		}
	}
	
	
}
