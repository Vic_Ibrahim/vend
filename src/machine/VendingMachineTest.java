package machine;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {
	
	private static double TOLERANCE = 0.001;
	
	@Test
	public void defaultStateIsOff() {
		VendingMachine machine = new VendingMachine();
		assertFalse(machine.isOn());
	}
	
	@Test
	public void turnsOn() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		assertTrue(machine.isOn());		
	}
	
	@Test
	public void turnsOff() {
		VendingMachine machine = new VendingMachine();
		Boolean onCheck = null;
		Boolean offCheck = null;
		
		machine.setOn();
		onCheck = machine.isOn();
		
		machine.setOff();
		offCheck = machine.isOn();
		
		assertTrue(onCheck);
		assertFalse(offCheck);
	}
	
	@Test
	public void currentInsertedBalanceIsZeroWhenturnedOn() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);	
	}
	
	@Test
	public void acceptsSingleCoin() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		
		assertEquals(coin, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptSingleCoinWhenOff() {
		VendingMachine machine = new VendingMachine();
		machine.setOff();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsMultipleSameValueCoins() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		machine.insertMoney(coin);
		machine.insertMoney(coin);
		
		assertEquals(coin*3 , machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsMultipleDifferentValueCoins() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double poundCoin = 1.0;
		double fiftyPenceCoin = 0.5;
		double twentyPenceCoin = 0.2;
		
		
		machine.insertMoney(poundCoin);
		machine.insertMoney(fiftyPenceCoin);
		machine.insertMoney(twentyPenceCoin);
		
		assertEquals(poundCoin+fiftyPenceCoin+twentyPenceCoin , machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void returnsCurrentInsertedMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		double refund = machine.refundCurrentInsertedBalance();
		
		assertEquals(coin, refund, TOLERANCE);
	}
	
	@Test
	public void currentInsertedBalanceResetAfterRefund() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		machine.refundCurrentInsertedBalance();
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsTenPenceCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.1;
		
		machine.insertMoney(coin);
		
		assertEquals(coin, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsTwentyPenceCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.2;
		
		machine.insertMoney(coin);
		
		assertEquals(coin, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsFiftyPenceCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.5;
		
		machine.insertMoney(coin);
		
		assertEquals(coin, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void acceptsPoundCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 1.0;
		
		machine.insertMoney(coin);
		
		assertEquals(coin, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptPennyCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.01;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptTwoPenceCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.02;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptsFivePenceCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 0.05;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptTwoPoundCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 2.0;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void doesNotAcceptPoundCoin(){
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		double coin = 5.0;
		
		machine.insertMoney(coin);
		
		assertEquals(0.0, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	/*@Test
	public void itemADispensed() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		int countBefore = machine.getItemACount();
		machine.dispenseItemA();
		int countAfter = machine.getItemACount();
		
		assertEquals(countBefore-1, countAfter);
	}*/
	
	@Test
	public void itemADispensedForCorrectMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		machine.setItemACount(1);
		
		machine.insertMoney(0.5);
		machine.insertMoney(0.1);
		
		int countBefore = machine.getItemACount();
		machine.vendItemA();
		int countAfter = machine.getItemACount();
		
		assertEquals(countBefore-1, countAfter);
	}
	
	@Test
	public void itemANotDispensedForInsufficientMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		machine.insertMoney(0.5);
		
		int countBefore = machine.getItemACount();
		machine.vendItemA();
		int countAfter = machine.getItemACount();
		
		assertEquals(countBefore, countAfter);
	}
	
	@Test
	public void itemADispensedWithCurrentBalanceUpdated() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		machine.setItemACount(1);
		
		machine.insertMoney(1.0);
		machine.vendItemA();
		
		assertEquals(0.4, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void itemBDispensedForCorrectMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		machine.setItemBCount(1);
		
		machine.insertMoney(1.0);
		
		int countBefore = machine.getItemBCount();
		machine.vendItemB();
		int countAfter = machine.getItemBCount();
		
		assertEquals(countBefore-1, countAfter);
	}
	
	@Test
	public void itemBNotDispensedForInsufficientMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		machine.insertMoney(0.5);
		
		int countBefore = machine.getItemBCount();
		machine.vendItemB();
		int countAfter = machine.getItemBCount();
		
		assertEquals(countBefore, countAfter);
	}
	
	@Test
	public void itemBDispensedWithCurrentBalanceUpdated() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		machine.setItemBCount(1);
		
		machine.insertMoney(1.0);
		machine.insertMoney(0.5);
		machine.vendItemB();
		
		assertEquals(0.5, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void itemCDispensedForCorrectMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		machine.insertMoney(1.0);
		machine.insertMoney(0.5);
		machine.insertMoney(0.2);
		
		int countBefore = machine.getItemCCount();
		machine.vendItemC();
		int countAfter = machine.getItemCCount();
		
		assertEquals(countBefore-1, countAfter);
	}
	
	@Test
	public void itemCNotDispensedForInsufficientMoney() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		machine.insertMoney(0.5);
		
		int countBefore = machine.getItemCCount();
		machine.vendItemC();
		int countAfter = machine.getItemCCount();
		
		assertEquals(countBefore, countAfter);
	}
	
	@Test
	public void itemCDispensedWithCurrentBalanceUpdated() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		
		machine.insertMoney(1.0);
		machine.insertMoney(1.0);
		machine.vendItemC();
		
		assertEquals(0.3, machine.getCurrentInsertedMoney(), TOLERANCE);
	}
	
	@Test
	public void itemAOnlyVendedIfAvailable() {
		VendingMachine machine = new VendingMachine();
		machine.setItemACount(10);
		machine.setOn();
		
		machine.insertMoney(0.5);
		machine.insertMoney(0.1);
		machine.vendItemA();
		
		assertEquals(0, machine.getCurrentInsertedMoney(), TOLERANCE);
		assertTrue(machine.getItemACount() == 9);
	}
	
	@Test
	public void itemANotVendedIfNotAvailable() {
		VendingMachine machine = new VendingMachine();
		machine.setItemACount(0);
		machine.setOn();
		
		machine.insertMoney(0.5);
		machine.insertMoney(0.1);
		machine.vendItemA();
		
		assertEquals(0.6, machine.getCurrentInsertedMoney(), TOLERANCE);
		assertTrue(machine.getItemACount() == 0);
	}
	
	@Test
	public void itemBOnlyVendedIfAvailable() {
		VendingMachine machine = new VendingMachine();
		machine.setItemBCount(10);
		machine.setOn();
		
		machine.insertMoney(1.0);
		machine.vendItemB();
		
		assertEquals(0, machine.getCurrentInsertedMoney(), TOLERANCE);
		assertTrue(machine.getItemBCount() == 9);
	}
	
	@Test
	public void itemBNotVendedIfNotAvailable() {
		VendingMachine machine = new VendingMachine();
		machine.setItemBCount(0);
		machine.setOn();
		
		machine.insertMoney(1.0);
		machine.vendItemB();
		
		assertEquals(1.0, machine.getCurrentInsertedMoney(), TOLERANCE);
		assertTrue(machine.getItemACount() == 0);
	}
}
