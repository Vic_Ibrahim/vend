package machine;

import java.util.HashMap;
import java.util.Map;

public enum Coin {
	ONE_PENCE		(1,false),
	TWO_PENCE		(2,false),
	FIVE_PENCE		(5,false),
	TEN_PENCE		(10,true),
	TWENTY_PENCE	(20,true),
	FIFTY_PENCE		(50,true),
	ONE_POUND		(100,true),
	TWO_POUND		(200,false),
	FIVE_POUND		(500,false);
	
	private final int pennyValue;
	private final boolean vendingMachineAccepted;
	private static Map<Integer, Coin> map = new HashMap<Integer, Coin>();
	
	Coin(int pennyValue, boolean vendingMachineAccepted) {
		this.pennyValue = pennyValue;
		this.vendingMachineAccepted = vendingMachineAccepted;
	}
	
	static {
		for (Coin coin: Coin.values()) {
			map.put(coin.pennyValue, coin);
		}
	}
	
	public int getPennyValue() {return pennyValue;}
	public boolean isVendingMachineAccepted() {return vendingMachineAccepted;}
	
	static Coin valueOf(int coin) {
		return map.get(coin);
	}
	
	static Coin valueOf(double coin) {
		Double pennyValue = coin*100;
		int pennies = pennyValue.intValue();
		return valueOf(pennies);
	}
	
	boolean isVendingMachineAccepted(int coin) {
		for(Coin c : Coin.values()) {
			if (c.getPennyValue() == coin) {
				return true;
			}
		}
		return false;
	}
	
	boolean isVendingMachineAccepted(double coin) {
		Double pennyValue = coin*100;
		int pennies = pennyValue.intValue();
		return isVendingMachineAccepted(pennies);
		
	}

}
